﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Agenda.Data;
using Agenda.Models;
using Microsoft.AspNetCore.Authorization;

namespace Agenda.Controllers
{
    public class PsiquiatrasController : Controller
    {
        private readonly AgendaDbContext _context;

        public PsiquiatrasController(AgendaDbContext context)
        {
            _context = context;
        }

        // GET: Psiquiatras
        public async Task<IActionResult> Index()
        {
            return View(await _context.Psiquiatras.ToListAsync());
        }

        // GET: Psiquiatras/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var psiquiatra = await _context.Psiquiatras
                .FirstOrDefaultAsync(m => m.PsiquiatraID == id);
            if (psiquiatra == null)
            {
                return NotFound();
            }

            return View(psiquiatra);
        }

        // GET: Psiquiatras/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Psiquiatras/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("PsiquiatraID,Nombre,Apellido,Cedula,Telefono")] Psiquiatra psiquiatra)
        {
            if (ModelState.IsValid)
            {
                _context.Add(psiquiatra);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(psiquiatra);
        }

        // GET: Psiquiatras/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var psiquiatra = await _context.Psiquiatras.FindAsync(id);
            if (psiquiatra == null)
            {
                return NotFound();
            }
            return View(psiquiatra);
        }

        // POST: Psiquiatras/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("PsiquiatraID,Nombre,Apellido,Cedula,Telefono")] Psiquiatra psiquiatra)
        {
            if (id != psiquiatra.PsiquiatraID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(psiquiatra);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PsiquiatraExists(psiquiatra.PsiquiatraID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(psiquiatra);
        }

        // GET: Psiquiatras/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var psiquiatra = await _context.Psiquiatras
                .FirstOrDefaultAsync(m => m.PsiquiatraID == id);
            if (psiquiatra == null)
            {
                return NotFound();
            }

            return View(psiquiatra);
        }

        // POST: Psiquiatras/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var psiquiatra = await _context.Psiquiatras.FindAsync(id);
            _context.Psiquiatras.Remove(psiquiatra);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PsiquiatraExists(int id)
        {
            return _context.Psiquiatras.Any(e => e.PsiquiatraID == id);
        }
    }
}
