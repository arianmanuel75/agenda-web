﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Agenda.Models;
using Newtonsoft.Json;
using Agenda.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Agenda.Controllers
{
    public class HomeController : Controller
    {
        private readonly AgendaDbContext _context;
        public HomeController(AgendaDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public JsonResult GetEvents()
        {
            var events =
                from a in _context.Citas
                join b in _context.Clientes on a.ClienteID equals b.ClienteID
                join c in _context.Psiquiatras on a.PsiquiatraID equals c.PsiquiatraID
                join d in _context.TipoPagos on a.TipoPagoID equals d.TipoPagoID
                select new
                {
                    Cliente = b.NombreCompleto,
                    Psiquiatra = c.NombreCompleto,
                    Pago = d.Descripcion,
                    Fecha_Cita = a.Fecha_Cita,
                    Fecha_Inicio = a.Fecha_Inicio,
                    Fecha_Fin = a.Fecha_Fin,
                    Descripcion = a.Descripcion
                };

            return Json(events.ToList());
        }

        //public async System.Threading.Tasks.Task<JsonResult> GetEventsAsync()
        //{
        //    var events = _context.Citas.Include(c => c.Cliente).Include(c => c.Psiquiatra).Include(c => c.TipoPago);

        //    return Json(await events.ToListAsync());
        //}

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
