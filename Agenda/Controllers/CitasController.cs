﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Agenda.Data;
using Agenda.Models;
using Microsoft.AspNetCore.Authorization;

namespace Agenda.Controllers
{
    public class CitasController : Controller
    {
        private readonly AgendaDbContext _context;

        public CitasController(AgendaDbContext context)
        {
            _context = context;
        }

        // GET: Citas
        public async Task<IActionResult> Index()
        {
            var agendaDbContext = _context.Citas.Include(c => c.Cliente).Include(c => c.Psiquiatra).Include(c => c.TipoPago);
            return View(await agendaDbContext.ToListAsync());
        }

        // GET: Citas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cita = await _context.Citas
                .Include(c => c.Cliente)
                .Include(c => c.Psiquiatra)
                .Include(c => c.TipoPago)
                .FirstOrDefaultAsync(m => m.CitaID == id);
            if (cita == null)
            {
                return NotFound();
            }

            return View(cita);
        }

        // GET: Citas/Create
        public IActionResult Create()
        {
            ViewData["Cliente"] = new SelectList(_context.Clientes, "ClienteID", "NombreCompleto");
            ViewData["Psiquiatra"] = new SelectList(_context.Psiquiatras, "PsiquiatraID", "NombreCompleto");
            ViewData["TipoPagoID"] = new SelectList(_context.TipoPagos, "TipoPagoID", "Descripcion");
            return View();
        }

        // POST: Citas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("CitaID,ClienteID,PsiquiatraID,Fecha_Cita,Fecha_Inicio,Fecha_Fin,Descripcion,TipoPagoID,Coste,Pago")] Cita cita)
        {
            if (ModelState.IsValid)
            {
                _context.Add(cita);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClienteID"] = new SelectList(_context.Clientes, "ClienteID", "Apellido", cita.ClienteID);
            ViewData["PsiquiatraID"] = new SelectList(_context.Psiquiatras, "PsiquiatraID", "Apellido", cita.PsiquiatraID);
            ViewData["TipoPagoID"] = new SelectList(_context.TipoPagos, "TipoPagoID", "Descripcion", cita.TipoPagoID);
            return View(cita);
        }

        // GET: Citas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cita = await _context.Citas.FindAsync(id);
            if (cita == null)
            {
                return NotFound();
            }
            ViewData["ClienteID"] = new SelectList(_context.Clientes, "ClienteID", "Apellido", cita.ClienteID);
            ViewData["PsiquiatraID"] = new SelectList(_context.Psiquiatras, "PsiquiatraID", "Apellido", cita.PsiquiatraID);
            ViewData["TipoPagoID"] = new SelectList(_context.TipoPagos, "TipoPagoID", "Descripcion", cita.TipoPagoID);
            return View(cita);
        }

        // POST: Citas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("CitaID,ClienteID,PsiquiatraID,Fecha_Cita,Fecha_Inicio,Fecha_Fin,Descripcion,TipoPagoID,Coste,Pago")] Cita cita)
        {
            if (id != cita.CitaID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(cita);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CitaExists(cita.CitaID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClienteID"] = new SelectList(_context.Clientes, "ClienteID", "Apellido", cita.ClienteID);
            ViewData["PsiquiatraID"] = new SelectList(_context.Psiquiatras, "PsiquiatraID", "Apellido", cita.PsiquiatraID);
            ViewData["TipoPagoID"] = new SelectList(_context.TipoPagos, "TipoPagoID", "Descripcion", cita.TipoPagoID);
            return View(cita);
        }

        // GET: Citas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var cita = await _context.Citas
                .Include(c => c.Cliente)
                .Include(c => c.Psiquiatra)
                .Include(c => c.TipoPago)
                .FirstOrDefaultAsync(m => m.CitaID == id);
            if (cita == null)
            {
                return NotFound();
            }

            return View(cita);
        }

        // POST: Citas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var cita = await _context.Citas.FindAsync(id);
            _context.Citas.Remove(cita);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CitaExists(int id)
        {
            return _context.Citas.Any(e => e.CitaID == id);
        }
    }
}
