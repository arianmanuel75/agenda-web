﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agenda.Models
{
    public class TipoPago
    {
        public int TipoPagoID { get; set; }

        [Required(ErrorMessage = "Debe introducir el nombre del tipo de pago")]
        public string Descripcion { get; set; }

        public bool Estado { get; set; }

        public ICollection<Cita> Cita { get; set; }
    }
}
