﻿using SistemaInventarioVentas.Models.validacionesCustomisadas;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agenda.Models
{
    public class Psiquiatra
    {
        public int PsiquiatraID { get; set; }

        [Required(ErrorMessage = "Debe introducir el nombre del psiquiatra")]
        [MinLength(3, ErrorMessage = "El nombre debe tener al menos 3 caracteres")]
        [MaxLength(20, ErrorMessage = "El nombre debe ser de 20 o menos caracteres")]
        public string Nombre { get; set; }

        [Required(ErrorMessage = "Debe introducir el apellido del psiquiatra")]
        [MinLength(3, ErrorMessage = "El apellido debe tener al menos 3 caracteres")]
        [MaxLength(20, ErrorMessage = "El apellido debe ser de 20 o menos caracteres")]
        public string Apellido { get; set; }

        public string NombreCompleto => string.Format("{0} {1}", Nombre, Apellido);

        //[ValidacionCedulaPsiquiatra]
        [Required(ErrorMessage = "Debe introducir el apellido del psiquiatra")]
        [RegularExpression(@"\b\d{3}\-?\d{7}\-?\d{1}\b", ErrorMessage = "La cedula no es valida")]
        public string Cedula { get; set; }

        [RegularExpression(@"\b\d{3}\s?\d{3}\-?\d{4}\b", ErrorMessage = "El telefono no es valido")]
        public string Telefono { get; set; }

        public ICollection<Cita> Cita { get; set; }
    }
}
