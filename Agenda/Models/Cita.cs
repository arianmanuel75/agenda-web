﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Agenda.Models
{
    public class Cita : IValidatableObject
    {
        public int CitaID { get; set; }

        [Display(Name = "Cliente")]
        public int ClienteID { get; set; }
        public virtual Cliente Cliente { get; set; }

        [Display(Name = "Psiquiatra")]
        public int PsiquiatraID { get; set; }
        public virtual Psiquiatra Psiquiatra { get; set; }

        [Display(Name = "Fecha")]
        [Required(ErrorMessage = "Debe introducir la fecha de la cita")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime Fecha_Cita { get; set; }

        [Display(Name = "Desde")]
        [Required(ErrorMessage = "Debe introducir la hora de inicio de la cita")]
        [DataType(DataType.Time)]
        public DateTime Fecha_Inicio { get; set; }

        [Display(Name = "Hasta")]
        [Required(ErrorMessage = "Debe introducir la hora de fin de la cita")]
        [DataType(DataType.Time)]
        public DateTime Fecha_Fin { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if (Fecha_Fin < Fecha_Inicio)
            {
                yield return
                  new ValidationResult(errorMessage : "Hasta debe ser mas tarde que Desde",
                    memberNames: new[] { "Fecha_Fin" });
            }
        }

        public string Descripcion { get; set; }

        [Display(Name = "Tipo de pago")]
        public int TipoPagoID { get; set; }
        public virtual TipoPago TipoPago { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "El precio debe ser mayor a 0")]
        [Required(ErrorMessage = "Debe introducir el coste de la cita")]
        public decimal Coste { get; set; }

        [Display(Name = "Pago?")]
        public bool Pago { get; set; }

    }
}
