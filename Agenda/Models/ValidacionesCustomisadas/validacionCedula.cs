﻿using Agenda.Data;
using Agenda.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaInventarioVentas.Models.validacionesCustomisadas
{
    public class ValidacionCedulaCliente : ValidationAttribute
    {
        private readonly AgendaDbContext _context;
        public ValidacionCedulaCliente()
        {
            _context = new AgendaDbContext();
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var cliente = (Cliente)validationContext.ObjectInstance;
            var clienteEnDB = _context.Clientes.SingleOrDefault(c => c.Cedula == cliente.Cedula);

            if (clienteEnDB != null && clienteEnDB.ClienteID != cliente.ClienteID)
            {
                return new ValidationResult("Ya existe un cliente con esta cedula");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

    public class ValidacionCedulaPsiquiatra : ValidationAttribute
    {
        private readonly AgendaDbContext _context;
        public ValidacionCedulaPsiquiatra()
        {
            _context = new AgendaDbContext();
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var psiquiatra = (Psiquiatra)validationContext.ObjectInstance;
            var psiquiatraEnDB = _context.Psiquiatras.SingleOrDefault(c => c.Cedula == psiquiatra.Cedula);

            if (psiquiatraEnDB != null && psiquiatraEnDB.PsiquiatraID != psiquiatra.PsiquiatraID)
            {
                return new ValidationResult("Ya existe un psiquiatra con esta cedula");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }
}