﻿using System;
using System.Collections.Generic;
using System.Text;
using Agenda.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Agenda.Data
{
    public class AgendaDbContext : DbContext
    {
        public AgendaDbContext()
        {
        }

        public AgendaDbContext(DbContextOptions<AgendaDbContext> options)
            : base(options)
        {
        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Psiquiatra> Psiquiatras { get; set; }
        public DbSet<TipoPago> TipoPagos { get; set; }
        public DbSet<Cita> Citas { get; set; }
    }
}
